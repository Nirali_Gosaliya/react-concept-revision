import React from 'react';

function ConditionalRenderLogicalOperator(props) {
    const newcar = props.carname
  return (
    <div>
        {/* if the newcar.length condition becomes false then the expression after the 
        && operator will not execute */}
        {newcar.length > 0 && 
            <h3>
                New Collection of car and its length is {newcar.length}
            </h3>
        }
    </div>
  )
}

export default ConditionalRenderLogicalOperator;