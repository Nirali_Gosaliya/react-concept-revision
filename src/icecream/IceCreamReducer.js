
import {ICE_CREAM} from './IceCreamtypes'

const initialState = {
    numoficecream : 100
}

const IceCreamReducer = (state=initialState,action) => {
    switch(action.type){
        case ICE_CREAM:
            return{
                numoficecream: state.numoficecream*4
            }
            default: return state
    }
}

export default IceCreamReducer;