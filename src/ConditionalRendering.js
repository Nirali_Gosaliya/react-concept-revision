import React from 'react';

function ConditionalRendering(props){
    const goal = props.isGoal
    if(goal){
        return <MissedGoal />
    }
        return <MadeGoal />
}
function MissedGoal(){
    return <h3>You missed it ! </h3>
}

function MadeGoal(){
    return <h3>Made Goal !</h3>
}

export default ConditionalRendering;