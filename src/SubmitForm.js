import React from 'react'

class SubmitForm extends React.Component{
    constructor(){
        super();
        this.state = {
            username: null,
            password: null
        }
    }
    handleSubmit = (e)=> {
        console.log(this.state)
        
    }
    render(){
        return(
            <div>
                <input type="text" name="username" onChange={(e)=>{this.setState({username: e.target.value})}}></input><br /><br />
                <input type="password" name="password" onChange={(e)=>{this.setState({password: e.target.value})}}></input><br /><br />
                <button type="submit" onClick={this.handleSubmit}>Submit</button>
            </div>
        )
    }
    
}

export default SubmitForm;