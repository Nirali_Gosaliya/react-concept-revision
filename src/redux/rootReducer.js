import { combineReducers } from "redux";
import CakeReducer from "../cake/CakeReducer";
import IceCreamReducer from "../icecream/IceCreamReducer";
import UserReducer from "../User/UserReducer";



const rootReducer = combineReducers({
    cake: CakeReducer,
    iceCream: IceCreamReducer,
    user: UserReducer
})


export default rootReducer;