import React,{useState} from 'react'

function HandleForm() {
    const [name,setName] = useState("")
    const [interest,setInterest] = useState("")
    const [tnc,setTnc] = useState("")


    function handlesubmit(e){
        e.preventDefault();
    }
  return (
    <div>
       <form>
            <input type = "text" placeholder='Enter Name' onChange={(e)=>setName(e.target.value)}></input><br /><br />
            <select onChange={(e)=>setInterest(e.target.value)}>
                <option value="Marvel">Marvel</option>
                <option value = "DC">DC</option>
            </select><br /><br />
            <input type="checkbox" onChange={(e)=>setTnc(e.target.checked)} disabled={true}></input><span>Agree with terms and condition</span><br /><br />
            <button type="submit" onClick={handlesubmit}>Submit</button>
       </form>
    </div>
  )
}

export default HandleForm;