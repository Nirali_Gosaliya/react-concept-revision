import React from 'react';

class ComponentDidUpdate extends React.Component{
    constructor(){
        super();
        this.state = {
            count: 0
        }
    }
    componentDidUpdate(preProps,preState,snapshot){
        console.warn("Component did update",snapshot);
        if(this.state.count<10){
            this.setState({count: this.state.count+1})
        }
    }
    render(){
        return(
            <div>
                <h3>Component Did Update{this.state.count}</h3>
                <button type="button" onClick={()=>this.setState({count: this.state.count+1})}>Click</button>
            </div>
        )
    }
}

export default ComponentDidUpdate;