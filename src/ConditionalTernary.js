import React,{useState} from 'react';

function ConditionalTernary() {
    const [loggedIn,setLoggedIn] = useState(false)
  return (
    <div>
        <button onClick={()=>setLoggedIn(!loggedIn)}>
            {loggedIn ? "Log Out" : "Log In"}
        </button>
        {loggedIn ? (
            <h2>Hello Nirali</h2>
        ): (
            <div>
                <h3>Please log in to continue</h3>
            </div>
        )
        }
    </div>
  )
}

export default ConditionalTernary;