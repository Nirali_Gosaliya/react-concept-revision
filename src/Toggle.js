import React from 'react'

function Toggle() {
    const [status,setStatus] = React.useState(true)
  return (
    <div>
        {
            status ? <h1>Hello Toggle</h1> : null
        }
        <button type="button" onClick={()=>setStatus(!status)}>Toggle</button>
        <PropsExample array={["hobby","travelling"]} info={{Name: "nirali",age: 21,profile: "Developer"}} />
        
    </div>
  )
}

function PropsExample(props) {
//  console.log(props.info);


    return (
      <div>
          <h3>My extra curricular is : {props.array.join(' ')}</h3>

          <h2>My name is {props.info.Name} {props.info.age} {props.info.profile} </h2>
      </div>
    )
  }

export default Toggle;




