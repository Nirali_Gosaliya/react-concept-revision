import React from 'react';

class StateClass extends React.Component{
    state = {
        data: "Darsh Shah"
    }
     handleName = () =>{
        this.setState({data:'Nirali'})
    }
   render(){
        return(
            <div>
                <p>{this.state.data}</p>
                <button type="button" onClick={this.handleName}>Click here</button>
            </div>
            
        )
    }
     
}

export default StateClass;