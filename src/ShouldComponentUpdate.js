import React from 'react';
import { findRenderedDOMComponentWithClass } from 'react-dom/test-utils';

class ShouldComponentUpdate extends React.Component{
    constructor(){
        super();
        this.state = {
            count: 0
        }
    }
    shouldComponentUpdate(){
        console.warn(this.state.count)
        return true;
    }
    render(){
    return(
        <div>
            <h3>Example of count : {this.state.count}</h3>
            <button onClick={()=>{this.setState({count: this.state.count+1})}}>Click Count</button>
        </div>
        )
    }
}

export default ShouldComponentUpdate;