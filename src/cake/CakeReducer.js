import { BUY_CAKE } from "./CakeTypes"

export  const initialState = {
    numofcakes: 10
}

const CakeReducer = (state=initialState,action) => {
    switch(action.type){
        case BUY_CAKE:
            return{
                ...state,
                numofcakes : state.numofcakes-action.payload
            }
            default: return state
    }
}

export default CakeReducer;