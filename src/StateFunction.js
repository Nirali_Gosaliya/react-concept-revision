import React,{useState} from 'react'

function StateFunction() {
    const [profile,setProfile] = useState("")

    const handleProfile=()=>{
        setProfile("Hiral Gosaliya: Developer")
    }
  return (
    <div>
        <p>Present {profile}</p>
        <button type="button" onClick={handleProfile}>Statefun</button>
    </div>
  )
}

export default StateFunction;