import React,{useState} from 'react';


function AsyncAwait(){   
    const [name,setName] = useState("Nirali")

     async function asyncFun(){
         let promise = new Promise(function(resolve){
             setTimeout(()=>{
                 resolve("fhlksfjal")
             },2000)
         })
         setName(await promise)
        //  console.log(result)
        //  console.log('hello');
     }
    return(
        <div>
            <button onClick={asyncFun}>{name}</button><br /><br />
            <AsyncAwait1 />
        </div>
    )
}

// another example
function AsyncAwait1(){
    async function myDisplay(){
        let prom = new Promise(function(resolve){
            resolve("I love you");
        })

        let value = await prom
        console.log(value);
    }
    return (
        <div>
            <button type="button" onClick={myDisplay}>Click Here</button>
        </div>
    )
}

export default AsyncAwait;