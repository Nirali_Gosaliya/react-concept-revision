import React from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {buycake} from '../cake/CakeAction'


function CakeContainerHook() {
  const numofcakes = useSelector(state=>state.cake.numofcakes)
  const dispatch = useDispatch()
  return (
    <div>
        <div>
            <h3>Number of cakes {numofcakes}</h3>
            <button onClick={()=>dispatch(buycake())}>Buy cake</button>
        </div>
    </div>
  )
}

export default CakeContainerHook;