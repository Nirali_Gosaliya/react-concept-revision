import React,{useReducer} from 'react'

const initialState = {count: 0}
const reducer = (state,action)=>{
    console.log(state,action)

    switch(action.type){
        case 'Increament':
            return {count: state.count+1}
        case 'Decreament':
            return {count: state.count-1}
        default:
            return state
    }
}
function UseReducerHook() {
    const [state,dispatch] = useReducer(reducer,initialState)
  return (
    <div>
        
        <div> Count : {state.count} </div>
       
        <button onClick={()=>dispatch({type: 'Increament'})}>Increament</button>
        <button onClick={()=>dispatch({type: 'Decreament'})}>Decreament</button>
    </div>
  )
}

export default UseReducerHook;