import React from 'react'
import {connect} from 'react-redux';
import {buycake} from '../cake/CakeAction'


function CakeContainer(props) {
  
  return (
   <>
    <div>
        <div>Number of cakes - {props.numofcakes}</div>
        <button onClick={props.buycake}>Buy cakes</button>
       
    </div>
   </>
  )
}

const mapStateToProps = state => {
    return{
        numofcakes: state.cake.numofcakes
    }
}

const mapDispatchToProps = dispatch => {
    return{
        buycake: ()=>dispatch(buycake())
       
    }
}



export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CakeContainer);