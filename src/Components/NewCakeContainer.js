import React,{useState} from 'react'
import {connect} from 'react-redux';
import {buycake} from '../cake/CakeAction'


function NewCakeContainer(props) {
  const [number,setNumber] = useState(1)
  return (
   <>
    <div>
        <div>Number of cakes - {props.numofcakes}</div>
        <input type="text" value={number} onChange={(e)=>setNumber(e.target.value)}></input>
        <button onClick={()=>props.buycake(number)}>Buy {number} cakes</button>
       
    </div>
   </>
  )
}

const mapStateToProps = state => {
    return{
        numofcakes: state.cake.numofcakes
    }
}

const mapDispatchToProps = dispatch => {
    return{
        buycake: (number)=>dispatch(buycake(number)),
       
    }
}



export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewCakeContainer);