import React from 'react'
import {buyicecream} from '../icecream/IceCreamAction'
import {connect} from 'react-redux'

function IceCreamContainer(props) {
  return (
        <>
            <div>
                <h3>Number of Ice creams - {props.numoficecream}</h3>
                <button onClick={props.buyicecream}>Buy Ice Creams</button>
            </div>
        </>
  )
}

const mapStateToProps = state=>{
    return{
        numoficecream: state.iceCream.numoficecream
    }
    
}

const mapDispatchToProps = dispatch =>{
    return{
        buyicecream: ()=>dispatch(buyicecream())
    }
}


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IceCreamContainer);