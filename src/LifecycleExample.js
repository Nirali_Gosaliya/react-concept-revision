import React from 'react';

class LifecycleExample extends React.Component{
    constructor(){
        super();
        this.state = {
           name : "Nirali Gosaliya",
           color: "lightgreen",
           fontSize: 20
        }
    }
    componentDidMount(){
        setTimeout(()=>{
            this.setState({name: "Hiral Gosaliya",color:"yellow"})
        },2000)
    }
    render(){
        return(
            <div>
                <p style={{fontSize: 30,
                color: this.state.color,
                fontSize: 50}}>{this.state.name}</p>
            </div>
        )
    }
}

export default LifecycleExample;