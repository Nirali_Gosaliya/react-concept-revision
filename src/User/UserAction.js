import { FETCH_USERS_REQUEST } from "./UserTypes"
import {FETCH_USERS_SUCCESS} from "./UserTypes"
import {FETCH_USERS_FAILURE} from "./UserTypes"
import axios from 'axios'


 const fetchUsersRequest = () => {
    return{
        type: FETCH_USERS_REQUEST
    }
}


 const fetchUserSuccess = (users) => {
    return{
        type: FETCH_USERS_SUCCESS,
        payload: users
    }
}


 const fetchUserFailure = (error) => {
    return{
        type: FETCH_USERS_FAILURE,
        payload: error
    }
}

export const fetchUsers = () => {
    return(dispatch)=>{
        axios.get("https://jsonplaceholder.typicode.com/users")
        .then(response => {
             
            dispatch(fetchUserSuccess(response.data))
        })
        .catch(error => {
            dispatch(fetchUserFailure(error.message))
        })

    }
}