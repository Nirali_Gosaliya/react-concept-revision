import logo from './logo.svg';
import './App.css';
import Toggle from './Toggle';
import StateClass from './StateClass';
import StateFunction from './StateFunction';
import ArrayDemo from './ArrayDemo';
import HandleForm from './HandleForm';
import SubmitForm from './SubmitForm';
import ConditionalRendering from './ConditionalRendering';
import  ConditionalRenderLogicalOperator from './ConditionalRenderLogicalOperator'
import ConditionalTernary from './ConditionalTernary'
import LifecycleExample from './LifecycleExample';
import ComponentDidUpdate from './ComponentDidUpdate';
import HigherOrderComponent from './HigherOrderComponent';
import AsyncAwait from './AsyncAwait';
import ShouldComponentUpdate from './ShouldComponentUpdate';
import UseReducerHook from './Components/UseReducerHook';
import CakeContainer from './Components/CakeContainer';
import {Provider} from 'react-redux'
import store from './redux/store'
import CakeContainerHook from './Components/CakeContainerHook';
import IceCreamContainer from './Components/IceCreamContainer';
import NewCakeContainer from './Components/NewCakeContainer';
import UserContainer from './User/UserContainer';





function App() {
  const cars = ["Audi","Toyota","BMW","Ford"]
  return (
    <div className="App">
     
      {/* <Toggle /> */}
      {/* <StateClass /> */}
      {/* <StateFunction /> */}
      {/* <ArrayDemo /> */}
      {/* <HandleForm /> */}
      {/* <SubmitForm /> */}
      {/* <ConditionalRendering isGoal={true}/> */}
      {/* <ConditionalRenderLogicalOperator carname={cars}/> */}
      {/* <ConditionalTernary /> */}
      {/* <LifecycleExample /> */}
      {/* <ComponentDidUpdate /> */}
      {/* <HigherOrderComponent /> */}
      {/* <AsyncAwait /> */}
      {/* <ShouldComponentUpdate /> */}
      {/* <UseReducerHook /> */}

      {/* Applying redux */}
      <Provider store={store}>
        <div>
        {/* <CakeContainer />
        <CakeContainerHook />
        <IceCreamContainer />
        <NewCakeContainer /> */}
        <UserContainer />
        </div>
      </Provider>
     
   
    </div>
  );
}

export default App;
