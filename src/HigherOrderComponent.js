import React,{useState} from 'react';

function HigherOrderComponent(){
    return(
        <div>
            <HOCRed cmp={Counter}></HOCRed>
            <HOCgreen cmp={Counter}></HOCgreen>
        </div>
    )
}
function HOCRed(props){
    return(
        <div>
            <h3><props.cmp /></h3>
        </div>
    )
}
function HOCgreen(props){
    return (
        <div>
            <h3><props.cmp /></h3>
        </div>
    )
}
function Counter(){
    const [count,setCount] = useState(0)
    return(
        <div>
            <h4>{count}</h4>
            <button type="button" onClick={()=>setCount(count+1)}>Update</button>
        </div>
    )
}

export default HigherOrderComponent;