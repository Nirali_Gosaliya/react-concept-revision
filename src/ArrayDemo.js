import React from 'react'

function ArrayDemo() {
  return (
    <div>
        <Map />
        <Filter />
        <Sort />
        <ForEach />
        <Concat />
        <Every />
        <Includes />
        <Reduce />
        <Find />
        <FindIndex />
        <IndexOf />
        <Fill />
        <Reverse />
        <Push />
        <Pop />
        <Unshift />
        <Shift />
        <Slice />
        <Splice />
        <ArrayFrom />
        <ArrayEntries />
        <ArrayOf />
        <ArrayKeys />
        <ArrayCopyWithin />
        {/* <RemoveDuplicates /> */}
        <Empty />
    </div>
  )
}

// Map method
function Map(){
    const array = [11,12,13,14,15]
    const mapped = array.map((element)=>element+10)
    console.log(mapped)
    return(
        <div>
            <h3>Map method</h3>
            <h3>Map method is used to iterate every element of an array </h3>
            <h3>Callback function is called for every element in an array</h3>
            <h2>{mapped.join(' ')}</h2>
            <hr />
        </div>
       
    )
}

// Filter Method 
function Filter(){
    const array = [100,2,300,4,500,6]
    const filtered = array.filter((item)=>item > 10)
    console.log(filtered)
    return(
        <div>
            <h3>Filter method</h3>
            <h3>Filter Method returns only those elements that passes the condition</h3>
            <h3>It returns the new array</h3>
            <h2>{filtered.join(' ')}</h2>
            <hr />
        </div>
       
    )
}

// Sort Method
// Returns the ascending array
// if we do (b-a) then it will return descending elements
// if we try to pass number capital letters and small letters than priority will be number , Capital ,small
function Sort(){
    const array = ["a","N",100,"4"]
    const sorted = array.sort()
    console.log(sorted)
    return(
        <div>
            <h3>Sort method</h3>
            <h3>Sort Method arrange the elements in either ascending order or descending order</h3>
            <h2>{sorted.join(' ')}</h2>
            <hr />
        </div>
       
    )
}

// ForEach Method
function ForEach(){
    const array = ["Nirali","Hiral","Jinal"]
     const itemforeach = array.forEach((item)=>{console.log(item)})
   
    return(
        <div>
            <h3>Sort method</h3>
            <h3>Iterate all elements in an array</h3>
            <h3>{itemforeach}</h3>
            <hr />
        </div>
       
    )
}

// concat Method
function Concat(){
    const array = ["Nirali","Hiral","Jinal"]
    const array2 = [100,'Darsh',4001]
     const concat_items = array.concat(array2)
     console.log(concat_items)
   
    return(
        <div>
            <h3>Concat Method</h3>
            <h3>Merge two or more array and returns a new array</h3>
            <h4>Merge array is : {concat_items.join(' ')}</h4>
            <hr />
        </div>
       
    )
}

// Every method
function Every(){
    const array = [2000,2005,3000]
     const every_items = array.every((item)=>item > 4000)
        console.log(every_items)
   
    return(
        <div>
            <h3>Concat Method</h3>
            <h3>Checks every element that passes the condition</h3>
            <h4>If all the elements passes the condition then it returns the true otherwise false</h4>
            <h3>{every_items}</h3>
            <hr />
        </div>
       
    )
}

// includes() method
function Includes(){
    const array = ['ajit','river',101]
     const include_items = array.includes(101)
        console.log(include_items)
   
    return(
        <div>
            <h3>Include Method</h3>
            <h3>Check whether the element contains the item or not</h3>
            <h4>Returns true or false</h4>
            <h3>{include_items}</h3>
            <hr />
        </div>
       
    )
}

// reduce method
function Reduce(){
    const array = [30,50,80]
     const reduce_items = array.reduce((prev,current)=>prev - current,0)
        console.log(reduce_items)
   
    return(
        <div>
            <h3>Reduce Method</h3>
            <h4>Returns a single value</h4>    
            <h5>reduced item is : {reduce_items}</h5>
            <hr />
        </div>
       
    )
}

// find method
// returns undefined if element is not found according to condition
function Find(){
    const array = [1001,9001,3001,5001,8001]
     const find_items = array.find((element)=>element > 4000)
        console.log(find_items)
   
    return(
        <div>
            <h3>Find Method</h3>
            <h4>Returns the first element that passes the test condition</h4>    
            <h5>Find  item is : {find_items}</h5>
            <hr />
        </div>
       
    )
}

// findIndex method
function FindIndex(){
    const array = [1001,9001,3001,5001,8001]
   
    return(
        <div>
            <h3>FindIndex Method</h3>
            <h4>Returns the index of first element that passes the test condition</h4>    
            <h5>{array.findIndex((element)=>element=== 9001)}</h5>
            <hr />
        </div>
       
    )
}

// indexOf method
function IndexOf(){
    const array = ["Nirali","Akshat",1000,1000]
   
    return(
        <div>
            <h3>IndexOf Method</h3>
            <h4>Returns the index of an element that ocuurence first</h4>    
            <h5>{array.indexOf(1000)}</h5>
            <hr />
        </div>
       
    )
}

// fill method
function Fill(){
    const array = ["Nirali","Akshat",120,"Jinal","Hiral"]
   
    return(
        <div>
            <h3>Fill Method</h3>
            <h4>fill the array with specified value from start to end</h4>
            <h4>{array.fill(500,1,4).join(' ')}</h4>
            <hr />
        </div>  
       
    )
}

// Reverse method
function Reverse(){
    const array = ["My","Name","is","nirali","Gosaliya",4000]
   
    return(
        <div>
            <h3>Reverse Method</h3>
            <h4>Reverse the elements of an array</h4>
            <h4>{array.reverse().join(' ')}</h4>
            <hr />
        </div>  
       
    )
}

// push method
function Push(){
    const array = ["My","Name","is","nirali","Gosaliya",4000]
   array.push("Ajit")
   console.log(array) 
   
    return(
        <div>
            <h3>Push Method</h3>
            <h4>Adds the element at the end of the array</h4>
            <h4>{array.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Pop method
function Pop(){
    const array = ["My","Name","is","nirali","Gosaliya",4000]
   array.pop();
   console.log(array) 
   
    return(
        <div>
            <h3>Push Method</h3>
            <h4>remove the element at the end of the array</h4>
            <h4>{array.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Unshift method
function Unshift(){
    const array = ["Heer","Jainik","Jay"]
   array.unshift("Riya shah");
   console.log(array) 
   
    return(
        <div>
            <h3>Unshift Method</h3>
            <h4>Add the element at the beginning of the array</h4>
            <h4>{array.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Shift method 
function Shift(){
    const array = ["Heer","Jainik","Jay"]
   array.shift();
   console.log(array) 
   
    return(
        <div>
            <h3>Shift Method</h3>
            <h4>Remove the element at the beginning of the array</h4>
            <h4>{array.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Slice method
// exclude the last number
// It returns the new array
function Slice(){
    const array = ["Heer","Jainik","Jay","Anisha","Sakshi","Manthan"]
   const slicearray = array.slice(2,5);
   console.log(slicearray) 
   
    return(
        <div>
            <h3>Slice Method</h3>
            <h4>Extract the element at specified index</h4>
            <h4>{slicearray.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Splice Method
function Splice(){
    const array = ["Heer","Jainik","Jay","Anisha","Sakshi","Manthan"]
   const splicearray = array.splice(2,0,"Karan");
   console.log(array) 
   
    return(
        <div>
            <h3>Splice Method</h3>
            <h4>Extract the element at specified index</h4>
            <h4>{array.join(',')}</h4>
            <hr />
        </div>  
       
    )
}

// Array from method
function ArrayFrom(){
    
   const arrayfrom = Array.from("Nirali")
   console.log(arrayfrom) 
   
    return(
        <div>
            <h3>Array From Method</h3>
            <h4>convert into array</h4>
            <h4>{arrayfrom.join(' ')}</h4>
            <hr />
        </div>  
       
    )
}

// Array of method
function ArrayOf(){
    
    const arrayof = Array.of(7)
    console.log(arrayof) 
    
     return(
         <div>
             <h3>Array of</h3>
             <h4>Array of method</h4>
             <h4>{arrayof.join(' ')}</h4>
             <hr />
         </div>  
        
     )
 }

//  Array entries method
function ArrayEntries(){
    
    const colors = ["red","pink",'yellow','brown']
    const colorarray= colors.entries()
    for (let i of colorarray){
        console.log(i)
    }
    console.log(colorarray)
    
     return(
         <div>
             <h3>Array Entries  Method</h3>
             <h4>returns the key value pair of an elements</h4>
             <hr />
         </div>  
        
     )
 }

//  Array.keys method
function ArrayKeys(){
    
    const colors = ["red","pink",'yellow','brown']
    const colorkeys= colors.keys()
   
    console.log([...colorkeys])
    
     return(
         <div>
             <h3>Array Keys Method</h3>
             <h4>returns the key  of an elements</h4>
              <hr />
         </div>  
        
     )
 }

//  Array copywithin
function ArrayCopyWithin(){
    
    const colors = [1,2,3,4,5,6,7]
   const newcolors = colors.copyWithin(3,1,3)
   
    console.log(newcolors)
    
     return(
         <div>
             <h3>Array Copy Within Method</h3>
             <h4>returns the copy of the element in the same array</h4>
             <h4>{newcolors.join(' ')}</h4>
              <hr />
         </div>  
        
     )
 }

// remove diplicates from an array
// function RemoveDuplicates(){
//     const arr = ["apple","banana","apple","orange","cherry","orange"]
//     return(
//         <div>
//             {arr.filter((value,index)=>arr.indexOf(value)===index)}
            
//         </div>
//     )
// }
function Empty(){
  let array = [1,2,3,4,5,6]
    array.length = 0
console.log(array)
    return(
        <div>
            <h3>Empty array</h3>
            <h4>{array}</h4>
            <hr />
        </div>
    )
}



export default ArrayDemo;